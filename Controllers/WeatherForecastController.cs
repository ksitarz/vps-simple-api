﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace vps_simple_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private static List<WeatherForecast> _container = new List<WeatherForecast>
        {
            new WeatherForecast(Guid.NewGuid(), new DateTime(2022, 1, 3), 3, "Bracing"),
            new WeatherForecast(Guid.NewGuid(), new DateTime(2022, 6, 12), 26, "Warm"),
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
            => _logger = logger;

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
            => _container; 

        [HttpPost]
        public IActionResult Post([FromBody] WeatherForecast model)
        {
            model.Uuid = Guid.NewGuid();
            _container.Add(model);
            return Created(model.Uuid.ToString(), model);
        }
    }
}
