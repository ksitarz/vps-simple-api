using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace vps_simple_api
{
    public class Startup
    {
        const string CorsClientUrlKey = "client-url";
        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment environment)
            => Configuration = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddUserSecrets<Program>()
                .AddEnvironmentVariables()
                .Build();

        public void ConfigureServices(IServiceCollection services)
            => services
                .AddControllers().Services
                .AddClientCors(Configuration[CorsClientUrlKey]);

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
            => app
                .UseHsts()
                .UseHttpsRedirection()
                .UseRouting()
                .UseAuthorization()
                .UseEndpoints(endpoints => endpoints.MapControllers())
                .UseClientCors();
    }
}
