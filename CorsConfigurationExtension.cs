using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace vps_simple_api
{
    public static class CorsConfigurationExtension
    {
        const string CorsPolicyName = "default-rule";

        public static IServiceCollection AddClientCors(this IServiceCollection services, string origin)
            => services.AddCors(options =>
                options.AddPolicy(CorsPolicyName,
                    builder =>
                    {
                        builder
                            .WithOrigins(origin)
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    }
                )
            );

        public static IApplicationBuilder UseClientCors(this IApplicationBuilder app)
            => app.UseCors(CorsPolicyName);
    }
}