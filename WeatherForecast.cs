using System;

namespace vps_simple_api
{
    public class WeatherForecast
    {
        public Guid Uuid { get; set; }

        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }

        public WeatherForecast() { }

        public WeatherForecast(Guid uuid, DateTime date, int temperatureC, string summary)
            => (Uuid, Date, TemperatureC, Summary)
            = (uuid, date, temperatureC, summary);
    }
}
